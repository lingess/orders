import OrderModel from './order'

export interface  OrderListViewModelInterface{
    orders: OrderModel[];
    getOrders(): OrderModel[];    
}

export interface  OrderListViewControllerInterface{}

export interface  OrderViewViewModelInterface{
    order: OrderModel;
    getOrder(orderID: string): OrderModel;
    cancelOrder(orderID: string): void; 
}

export interface  OrderViewViewControllerInterface{}

export interface  OrderUpdateViewModelInterface{
    order: OrderModel;
    getOrder(orderID: string): OrderModel;
    putOrder(orderID: string): void;
    cancelOrder(orderID: string): void; 
}

export interface  OrderUpdateViewControllerInterface{
    setPickUpLocation(location: string): void;
    setDropOffLocation(location: string): void;
    setPickUpDate(date: string): void;
    setDropOffDate(date: string): void;
}