import {observable, action, makeObservable} from 'mobx' 

class OrderModel {
    orderID: number
    orderStatus: string
    vehicleType: string
    pickUpLocation: string
    dropOffLocation: string
    pickUpDate: string
    dropOffDate: string

    constructor(orderID: number = 1, orderStatus: string = 'Reserved', vehicleType: string = '', pickUpLocation: string = '', dropOffLocation: string = '', pickUpDate: string = '', dropOffDate: string = ''){
        makeObservable(this, {​​​​
            orderID: observable,
            orderStatus: observable,
            vehicleType: observable,
            pickUpLocation: observable,
            dropOffLocation: observable,
            pickUpDate: observable,
            dropOffDate: observable
        }​​​​)
        this.orderID = orderID
        this.orderStatus = status
        this.vehicleType = vehicleType
        this.pickUpLocation = pickUpLocation
        this.dropOffLocation = dropOffLocation
        this.pickUpDate = pickUpDate
        this.dropOffDate = dropOffDate
    }
}

export default OrderModel